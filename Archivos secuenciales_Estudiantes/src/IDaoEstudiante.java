import java.io.IOException;
public interface IDaoEstudiante extends IDao<Estudiante>{
    Estudiante[] findByApellido(String apellido) throws IOException;
    Estudiante[] findByNombre(String nombre) throws IOException;
    Estudiante findById(int id) throws IOException;
    Estudiante findByCarnet(String carnet) throws IOException;
}
