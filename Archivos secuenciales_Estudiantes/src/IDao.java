import java.io.IOException;

import java.io.IOException;

public interface IDao<T> {
    void save(T t) throws IOException;
    void update(T t) throws IOException;
    void delete(T t) throws IOException;
    T[] findAll() throws IOException;
    
}
