import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
public class App {
    public static void main(String[] args) {

        Estudiante estudiante = null;
        EstudianteImpl eimpl= null;
        
        int opc = 0;

        Scanner entrada = new Scanner(System.in);

        do{
            try{
                menu();
                System.out.print("opc:");
                opc = entrada.nextInt();
                eimpl = new EstudianteImpl();

                switch (opc) {
                    case 1:
                    int id;
                    String nombres;
                    String apellidos;
                    String carnet;
                    int edad;

                    System.out.println("Ingrese el id");
                    id = entrada.nextInt();

                    entrada.nextLine();

                    System.out.print("Ingrese el nombre:\t");
                    nombres = entrada.nextLine();

                    System.out.println("\nIngrese apellidos:\t");
                    apellidos = entrada.nextLine();

                    System.out.println("\nIngrese el carnet:\t");
                    carnet = entrada.nextLine();

                    System.out.println("\nIngrese la edad:\t");
                    edad = entrada.nextInt();
                    estudiante = new Estudiante(id,nombres,apellidos,carnet,edad);
                    eimpl.save(estudiante);
                        break;
                
                    case 2:
                    System.out.println("Ingrese el ID del estudiante que desea editar");

                    int id2 = entrada.nextInt();
                    estudiante = eimpl.findById(id2);
                    eimpl.update(estudiante);
                        break;

                    case 3:
                        break;

                    case 4:
                    System.out.println("Ingrese el Id del estudiante");
                    int idfind = entrada.nextInt();
                    estudiante = eimpl.findById(idfind);
                    System.out.format("%10s %20s %20s %15s %10s\n", "Id","Nombres","Apeliidos","Carnet","Edad");
                    print(estudiante);

                        break;

                    case 5:
                    System.out.print("Ingrese el nombre del estudiante a buscar: ");
                    String nombres2;
                    Estudiante [] nombresfind = null;
                    entrada.nextLine();
                    nombres2 = entrada.nextLine();

                    nombresfind = eimpl.findByNombre(nombres2);

                    if (nombresfind == null){
                        System.out.println("Estudiante no encontrado");
                    }else{
                        System.out.format("%10s %20s %20s %15s %10s\n", "Id","Nombres","Apeliidos","Carnet","Edad");
                        for (Estudiante e : nombresfind){
                            print(e);
                        }
                    }
                        break;

                    case 6:
                    String apellidofind;
                    Estudiante [] apellido = null;

                    System.out.print("Ingrese el apellido a buscar: ");
                    entrada.nextLine();
                    apellidofind = entrada.nextLine();
                    apellido = eimpl.findByApellido(apellidofind);

                    
                    if (apellido == null){
                        System.out.println("Estudiante no encontrado");
                    }else{
                        System.out.format("%10s %20s %20s %15s %10s\n", "Id","Nombres","Apeliidos","Carnet","Edad");
                        for (Estudiante e : apellido){
                            print(e);
                        }
                    }

                        break;

                    case 7:
                    String carnetFind;
                    entrada.nextLine();
                    System.out.println("Ingrese el carnet a buscar: ");
                    carnetFind = entrada.nextLine();

                    estudiante = eimpl.findByCarnet(carnetFind);

                    if (estudiante == null){
                        System.out.println("Estudiante no encontrado");
                    }else{
                        System.out.format("%10s %20s %20s %15s %10s\n", "Id","Nombres","Apeliidos","Carnet","Edad");
                        print(estudiante);
                    }

                        break;

                    case 8:
                    System.out.format("%10s %20s %20s %15s %10s\n", "Id","Nombres","Apeliidos","Carnet","Edad");
                    for (Estudiante e : eimpl.findAll()){
                        print(e);
                    }
                        break;

                    case 9:
                    System.exit(0);
                        break;
                    
                    default:
                    System.out.println("Opcion no valida !!!");
                }
            }catch (FileNotFoundException ex){

            }catch (IOException ex){
                
            }finally{
                try{
                    if (eimpl != null){
                        eimpl.close();
                    }
                }catch (IOException ex){

                }
            }
        }while (opc !=9);
        
        entrada.close();
    }

    public static void menu(){
        System.out.println("Menu de Opciones para el manejo de Estudiantes");
        System.out.println("1. Registrar Estudiate");
        System.out.println("2. Editar Estudiante");
        System.out.println("3. Eliminar Estudiante");
        System.out.println("4. Buscar Estudiante por Id");
        System.out.println("5. Buscar Estudiante por Nombres");
        System.out.println("6. Buscar Estudiante por Apellidos");
        System.out.println("7. Buscar Estudiante por carnet");
        System.out.println("8. Listar todos los Estudiantes");
        System.out.println("9. Salir");
    }

    public static void print(Estudiante e){  //Cabecera y datos a mostrar
        System.out.format("%10d %20s %20s %15s %10d\n", e.getId(),e.getNombres(),e.getApellidos(),e.getCarnet(),e.getEdad());
    }

}
