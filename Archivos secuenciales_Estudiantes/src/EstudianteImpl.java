import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class EstudianteImpl implements IDaoEstudiante {
    FileOutputStream fos;
    private DataOutputStream dos;
    private DataInputStream dis;
    private File file ;
    private static final String FILENAME = "Estudiantes.dat";
    static Scanner entrada = new Scanner(System.in);

    public EstudianteImpl() throws IOException{
        load();
    }

    private void load() throws FileNotFoundException{
        file = new File(FILENAME);
        dos = new DataOutputStream(new FileOutputStream(file,true));
        dis = new DataInputStream(new FileInputStream(file));
    }

    private void updateDataBase() throws FileNotFoundException {
        fos = new FileOutputStream(file,false);
        dos = new DataOutputStream(fos);
    }

    @Override
    public void save(Estudiante t) throws IOException {
        if (t == null || dos == null){
            return;                                       //Se verifica si el objeto que se le esta pasando es diferente de null o el objeto
        }                                                 //"dos" es diferente de null

        dos.writeInt(t.getId());
        dos.writeUTF(t.getNombres());                     //Se manda a llamar los metodos correspondientes para los tipos de datos
        dos.writeUTF(t.getApellidos());                   //writeInt(int) para enteros ; writeUTF(String) para cadenas.
        dos.writeUTF(t.getCarnet());
        dos.writeInt(t.getEdad());
    }

    private Estudiante[] addEstudiante(Estudiante e, Estudiante[] temp){ //Arreglo Dinamico para agregar Estudiantes
        if (temp == null){
            temp = new Estudiante[1];
            temp[temp.length-1] = e;
            return temp;
        }

        temp = Arrays.copyOf(temp,temp.length+1);
        temp[temp.length-1] = e;
        return temp;
    }

    @Override
    public void update(Estudiante t) throws IOException {
        Estudiante temp[] = new Estudiante[findAll().length];
        temp = findAll();
        String nombres;
        String apellidos;
        String carnet;
        int edad;

        if (t == null){
            return;
        }

        
        

        System.out.print("Ingrese el nombre:\t");
        nombres = entrada.nextLine();

        System.out.print("Ingrese apellidos:\t");
        apellidos = entrada.nextLine();

        System.out.print("Ingrese el carnet:\t");
        carnet = entrada.nextLine();

        System.out.print("Ingrese la edad:\t");
        edad = entrada.nextInt();

        t.setNombres(nombres);
        t.setApellidos(apellidos);
        t.setCarnet(carnet);
        t.setEdad(edad);
        
        updateDataBase();

        for (Estudiante e2 : temp) {
            if (t.getId() == e2.getId()) {
                save(t);
            } else {
                save(e2);    
            }
        }

        close();

        System.out.println("Estudiante actualizado exitosamente\n");
    }

    @Override
    public void delete(Estudiante t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public Estudiante[] findAll() throws IOException {    //Se verifica si el archivo tiene bytes para leer. Si hay bytes para leer significa
        Estudiante[] estudiantes = null;                  //que hay para 1 registro de un estudiante dentro y se almacena en un arreglo de Estudiantes
        Estudiante e;

        while(dis.available() > 0){
            e = new Estudiante();
            e.setId(dis.readInt());
            e.setNombres(dis.readUTF());
            e.setApellidos(dis.readUTF());
            e.setCarnet(dis.readUTF());
            e.setEdad(dis.readInt());
            estudiantes = addEstudiante(e,estudiantes);   //Implementacion del Arreglo Dinamico
        }

        return estudiantes;
    }

    public void close() throws IOException{
        if (dos != null){
            dos.close();
        }

        if (dis != null){
            dis.close();
        }
    }

    @Override
    public Estudiante[] findByApellido(String apellido) throws IOException {
        Estudiante e = null;
        Estudiante[] e2 = null;

        while(dis.available() > 0){
            e = new Estudiante();
            e.setId(dis.readInt());
            e.setNombres(dis.readUTF());
            e.setApellidos(dis.readUTF());
            e.setCarnet(dis.readUTF());
            e.setEdad(dis.readInt());

            if (e.getApellidos().equals(apellido)){
                e2 = addEstudiante(e, e2);
            }
        }
        close();
        return e2;
    }

    @Override
    public Estudiante[] findByNombre(String nombre) throws IOException {
        Estudiante e = null;
        Estudiante[] e2 = null;
        
        while(dis.available() > 0){
            e = new Estudiante();
            e.setId(dis.readInt());
            e.setNombres(dis.readUTF());
            e.setApellidos(dis.readUTF());
            e.setCarnet(dis.readUTF());
            e.setEdad(dis.readInt());

            if (e.getNombres().equals(nombre)){
                e2 = addEstudiante(e, e2);
            }
        }
        close();
        return e2;
    }

    @Override
    public Estudiante findById(int id) throws IOException {
        Estudiante e = null;

        while(dis.available() > 0){
            e = new Estudiante();
            e.setId(dis.readInt());
            e.setNombres(dis.readUTF());
            e.setApellidos(dis.readUTF());
            e.setCarnet(dis.readUTF());
            e.setEdad(dis.readInt());

            if (id == e.getId()){
                return e;
            }else{
                e = null;
            }
        }
        close();
        return e;
    }

    @Override
    public Estudiante findByCarnet(String carnet) throws IOException{
        Estudiante e = null;

        while(dis.available() > 0){
            e = new Estudiante();
            e.setId(dis.readInt());
            e.setNombres(dis.readUTF());
            e.setApellidos(dis.readUTF());
            e.setCarnet(dis.readUTF());
            e.setEdad(dis.readInt());

            if (carnet.equals(e.getCarnet())){
                return e;
            }else{
                e = null;
            }
        }
        close();
        return e;
    }

}
