package Idao;

import java.io.IOException;

public interface Idao {
    void addText(String text) throws IOException;
    void update();
    String ver() throws IOException;
    
}