package Poo;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Archivo {
    private File file;
    private FileInputStream fis;
    private FileOutputStream fos;
    private DataInputStream dis;
    private DataOutputStream dos;
    private final String NAMEDATA = "Texto.dat";

    public Archivo() throws FileNotFoundException {
        load();
    }

    private void load() throws FileNotFoundException {

        /*if (file.exists() == false) {
            file = new File(NAMEDATA);
        }*/

        file = new File(NAMEDATA);

        fos = new FileOutputStream(file,true);
        fis = new FileInputStream(file);

        dos = new DataOutputStream(fos);
        dis = new DataInputStream(fis);
    }

    private void close() throws IOException {
        if (fis != null){
            fis.close();
        }

        if(fos != null){
            fos.close();
        }

        if(dis != null){
            dis.close();
        }

        if(dos != null){
            dos.close();
        }
    }

    public DataInputStream getInstanceDis() {
        return dis;
    }

    public DataOutputStream getInstaceDos() {
        return dos;
    }
}
