package Implement;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import Idao.Idao;
import Poo.Archivo;

public class Implement implements Idao {
    Archivo file;
    DataOutputStream dos;
    DataInputStream dis;

    public Implement() throws FileNotFoundException {
        file = new Archivo();
        this.dos = file.getInstaceDos();
        this.dis = file.getInstanceDis();
    }

    @Override
    public void addText(String text) throws IOException {
        if (text == "" || dos == null){
            return;
        }

        dos.writeUTF(text);
    }

    @Override
    public void update() {

    }

    @Override
    public String ver() throws IOException {
        String text=null;

        while(dis.available() > 0){
            text += dis.readUTF();
        }
        
        return text;
    }
    
}
