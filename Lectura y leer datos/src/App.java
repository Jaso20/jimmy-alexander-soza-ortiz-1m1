import java.util.Scanner;

import Service.ArchivoService;

public class App {
    public static void main(String[] args) throws Exception {
        int op = 0;
        Scanner sc = new Scanner(System.in);

        ArchivoService archivoService = new ArchivoService(sc);
        System.out.println("");


        do {
        menu();
        op = sc.nextInt();
        switch (op) {
            case 1:
                archivoService.agregarText();
                break;
        
            case 2:
            archivoService.ver();
                break;

            case 3:

                break;

            case 4:
            System.out.println("Cerrando programa . . .");
            sc.close();
               break;

            default:
            System.out.println("Menu inexistente!");
                break;
        }
        } while (op !=4);

    }

    public static void menu(){
        System.out.println(" --- MENU ---");
        System.out.println("1) Escribir");
        System.out.println("2) Leer");
        System.out.println("3) Editar");
        System.out.println("4) Salir\n");
    }
}
