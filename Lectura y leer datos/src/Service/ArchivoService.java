package Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import Implement.Implement;

public class ArchivoService {
    Scanner scan;
    Implement implement;

    public ArchivoService(Scanner scan) throws FileNotFoundException {
        this.scan = scan;
        implement = new Implement();
    }

    public void agregarText() throws IOException {
        String text;
        scan.nextLine();
        System.out.println("Ingrese lo que desea en el archivo");
        text = scan.nextLine();

        implement.addText(text);
        System.out.println("Texto agregado satisfactoriamente!!!");
    }

    public void ver() throws IOException {
        String text = implement.ver();
        System.out.println("En el archivo se encuentra:\n");
        System.out.println(text);
    }

    public void update() {
        
    }
    
}
