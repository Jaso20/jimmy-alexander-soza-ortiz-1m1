import java.util.List;

public interface Idao<T> {
    void add(T t);
    void update(T t);
    void delete(T t);
    List<T> findAll(T t);
}
