import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.DataOutputStream;
import java.io.DataInputStream;

public class FileConnection {
    File file;
    final String FILENAME = "Registro.dat";
    FileOutputStream fos;
    FileInputStream fis;

    DataOutputStream dos;
    DataInputStream dis;

    public FileConnection() throws FileNotFoundException {
        load();
    }

    private void load() throws FileNotFoundException {
        if (!file.exists()) {
            file = new File(FILENAME);
        } else {
            file = new File(FILENAME);
        }

        if (dos == null || dis == null) {
            dos = new DataOutputStream(new FileOutputStream(file, true));
            dis = new DataInputStream(new FileInputStream(file));
        } else {
            fos = new FileOutputStream(file);
            fis = new FileInputStream(file);

            dos = new DataOutputStream(fos);
            dis = new DataInputStream(fis);
        }
    }

    public DataOutputStream getDataOutputStream(File file) throws FileNotFoundException {
        if (dos == null) {
            dos = new DataOutputStream(new FileOutputStream(file));
        }

        return dos;
    }

    public DataInputStream getDataInputStream(File file) throws FileNotFoundException {
        if (dis == null) {
            dis = new DataInputStream(new FileInputStream(file));
        }

        return dis;
    }

    public void close() throws IOException {
        if (fos != null){
            fos.close();
            fos = null;
        }

        if (fis != null){
            fis.close();
            fis = null;
        }

        if (dos != null){
            dos.close();
            dos = null;
        }

        if (dis != null){
            dis.close();
            dis = null;
        }
    }
}
