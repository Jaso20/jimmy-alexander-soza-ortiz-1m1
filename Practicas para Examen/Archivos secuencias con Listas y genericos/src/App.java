import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int op = 0;

        do {
            menu();
            System.out.println("Seleccione una opción");
            op = sc.nextInt();

            switch (op) {
                case 1:
                    
                    break;

                case 2:
                    break;

                case 3:
                    break;

                case 4:
                    break;

                case 5:
                    break;

                case 6:
                    break;

                case 7:
                    break;

                case 8:
                    break;

                case 9:
                    break;

                case 10:
                sc.close();
                System.out.println("Cerrando programa. . .");
                    break;

                default:
                System.out.println("Menu inexistente ¿?");
                    break;
            }
        } while (op != 10);
    }

    public static void menu() {
        System.out.println("-----\tMenu\t-----");
        System.out.println("1) Agregar");
        System.out.println("2) Editar");
        System.out.println("3) Eliminar");
        System.out.println("4) Visualizar todos");
        System.out.println("5) Buscar por ID");
        System.out.println("6) Buscar por nombres");
        System.out.println("7) Buscar por apellido");
        System.out.println("8) Buscar por Carnet");
        System.out.println("9) Buscar por rango de edad");
        System.out.println("10) Salir\n");
    }
}
