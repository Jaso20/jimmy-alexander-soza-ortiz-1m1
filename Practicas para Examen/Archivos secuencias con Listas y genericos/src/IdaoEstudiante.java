import java.util.List;

public interface IdaoEstudiante extends Idao<Estudiante> {
    Estudiante findId(int id);
    List<Estudiante> findName(String nombre);
    List<Estudiante> findApellido(String apellido);
    Estudiante findCarnet(String carnet);
    List<Estudiante> findEdad(int edad);
}
