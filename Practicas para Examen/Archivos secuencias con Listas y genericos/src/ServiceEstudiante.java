import java.io.FileNotFoundException;
import java.util.Scanner;

public class ServiceEstudiante {
    Scanner scan;
    ImplementEstudiante iEstudiante;

    public ServiceEstudiante(Scanner scan) throws FileNotFoundException {
        this.scan = scan;
        iEstudiante = new ImplementEstudiante();
    }

    public void add() {
        int id;
        String nombre;
        String apellido;
        int edad;
        String carnet;
        Estudiante e;

        id = iEstudiante.setId();

        scan.nextLine();
        System.out.print("Nombre del estudiante");
        nombre = scan.nextLine();

        System.out.print("Apellido del estudiante: ");
        apellido = scan.nextLine();

        System.out.print("Edad del estudiante: ");
        edad = scan.nextInt();

        System.out.println("Carnet del estudiante: ");
        carnet = scan.nextLine();

        e = new Estudiante(id, nombre, apellido, edad, carnet);
        iEstudiante.add(e);

    }

    public void update(){

    }

    public void delete(){
        
    }

    public void findAll(){

    }

    public void findId(){

    }

    public void findName(){

    }
    
    public void findApellido(){

    }

    public void findCarnet(){

    }

    public void findEdad(){

    }


}
