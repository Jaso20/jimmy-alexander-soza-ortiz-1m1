import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {

        final String dataname = "Prueba.txt";
        Scanner sc = new Scanner(System.in);
        File file = new File(dataname);
        FileInputStream fis = null;
        FileOutputStream fos = null;

        System.out.println("Presionar ENTER");
        sc.nextLine();

        String texto;
        String text = "";
          
        System.out.println("Escriba una linea de texto");
        texto = sc.nextLine();

        try {
            fos(fos, texto, file);   // Metodo que escribe la linea de texto al archivo
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        System.out.println("El texto se escribio correctamente\n");

        System.out.println("Dentro del archivo se encuentra ...");

       
        try {
            text = fis(file, fis, text);  //Devuelve lo que se encuentra dentro del archivo de texto
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        System.out.println(text);

        try {
            close(fos, fis);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void close(FileOutputStream fos, FileInputStream fis) throws IOException {
        if (fos != null) {
            fos.close();
        }

        if (fis != null) {
            fis.close();
        }
    }

    private static String fis(File file, FileInputStream fis, String text) throws IOException { // Bytes del txt al usuario
        fis = new FileInputStream(file);
        int n = 0;
        while( (n = fis.read()) != -1){
            text += (char) n;
        }

        fis.close();
        
        return text;
    }

    public static void fos(FileOutputStream fos, String texto, File file) throws IOException { //Bytes del usuario el txt
        fos = new FileOutputStream(file, false);  //La condicion false sobreescribe en el archivo
        fos.write(texto.getBytes());

        fos.close();
    }
}

/*El fos sirve para escribir en el archivo, el fis para recibir bytes del archivo txt*/
