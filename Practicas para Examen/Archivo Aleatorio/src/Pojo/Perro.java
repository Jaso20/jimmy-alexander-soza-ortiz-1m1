package Pojo;

import java.time.LocalDate;

public class Perro {
    private int id; // => 4
    private String nombre; // => 20 => 20*2 + 3 = 43
    private String dueño; // => 20 => 20*2 + 3 = 43
    private String color; // => 10 => 10*2 + 3 = 23
    private double peso; // => 8
    private int edad; // => 4
    private Estado estado; // => 4
    private LocalDate consulta;    // => 8 TOTAL = 137 Bytes

    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDueño() {
        return dueño;
    }

    public void setDueño(String dueño) {
        this.dueño = dueño;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public LocalDate getConsulta() {
        return consulta;
    }

    public void setConsulta(LocalDate consulta) {
        this.consulta = consulta;
    }

    public Perro() {
    }

    public Perro(int id, String nombre, String dueño, String color, double peso, int edad, Estado estado,
            LocalDate consulta) {
        this.id = id;
        this.nombre = nombre;
        this.dueño = dueño;
        this.color = color;
        this.peso = peso;
        this.edad = edad;
        this.estado = estado;
        this.consulta = consulta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Perro(String nombre, String dueño, String color, double peso, int edad, Estado estado, LocalDate consulta) {
        this.nombre = nombre;
        this.dueño = dueño;
        this.color = color;
        this.peso = peso;
        this.edad = edad;
        this.estado = estado;
        this.consulta = consulta;
    }

    
    
    
}
