package IDao;

import java.time.LocalDate;
import java.util.List;
import java.util.TreeSet;

import Pojo.Estado;
import Pojo.Perro;

public interface IDaoPerro extends IDao<Perro> {
    Perro findById(int id);
    List<Perro> findByName(String name);
    List<Perro> findByDueno(String dueno);
    TreeSet<Perro> findByEdad(int edad);
    List<Perro> findByEstado(Estado estatus);
    List<Perro> consulta(LocalDate consulta);
}
