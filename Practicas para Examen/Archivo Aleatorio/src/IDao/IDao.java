package IDao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface IDao<T> {
    void create(T t) throws FileNotFoundException, IOException;
    void update(T t);
    void delete(T t);
    List<T> findAll() throws FileNotFoundException, IOException;
}
