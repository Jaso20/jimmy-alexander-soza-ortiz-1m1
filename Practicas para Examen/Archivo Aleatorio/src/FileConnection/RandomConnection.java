package FileConnection;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class RandomConnection {
    private RandomAccessFile raf;
    private File file;

    public RandomConnection(File file) {
        this.file = file;
    }

    public RandomAccessFile getConnection() throws FileNotFoundException {
        if (raf == null) {
            raf = new RandomAccessFile(file, "rw");
        }

        return raf;
    }

    public void close() throws IOException {
        if (raf != null){
            raf.close();
            raf = null;
        }
    }
}
