package Services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

import IDao.IDaoPerro;
import Implements.PerroImplements;
import Pojo.Estado;
import Pojo.Perro;

public class PerroService {
    private Scanner sc;
    private PerroImplements pImplements;

    public PerroService(Scanner sc) throws FileNotFoundException, IOException {
        this.sc = sc;
        pImplements = new PerroImplements();
    }

    
    public void create() throws FileNotFoundException, IOException {
        String nombre;
        String dueño;
        String color;
        double peso;
        int edad;
        int estado = 0;
        String consulta;

        sc.nextLine();
        System.out.print("Ingrese el nombre del perro: ");
        nombre = sc.nextLine();

        System.out.print("Ingrese el dueño del perro: ");
        dueño = sc.nextLine();

        System.out.print("Ingrese el color del perro");
        color = sc.nextLine();

        System.out.print("Ingrese el peso del perro");
        peso = sc.nextDouble();

        System.out.print("Ingrese la edad del perro");
        edad = sc.nextInt();

        do {
            System.out.print("Ingrese el estado del perro");
            estado = sc.nextInt();
            
        } while (estado < 1 || estado > 4);

        System.out.println("Fecha de Consulta [dd/MM/YYYY]");
        consulta = sc.next();

        Perro p = new Perro(nombre, dueño, color, peso, edad, Estado.values()[estado - 1], LocalDate.parse(consulta,DateTimeFormatter.ofPattern("dd/MM/YYYY")));

        pImplements.create(p);
    }

    
    public void update() {
       
    }

    
    public void delete() {
       
    }

    
    public void findAll() {
       
    }

    
    public void findById(int id) {
       
    }

    
    public void findByName(String name) {
       
    }

    
    public void findByDueno(String dueno) {
       
    }

    
    public void findByEdad(int edad) {
       
    }

    
    public void findByEstado(Estado estatus) {
       
    }

    
    public void consulta(LocalDate consulta) {
       
    }

}
