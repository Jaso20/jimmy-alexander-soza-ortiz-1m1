import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int op = 0, opPerro = 0;

        do {
            System.out.println("\t--- MENU ---");
            menuPrincipal();
            System.out.print("Seleccionar: ");
            op = sc.nextInt();

            switch (op) {
                case 1:
                do {
                    menuPerro();
                    System.out.print("Seleccionar: ");
                    opPerro = sc.nextInt();   
                    switch (opPerro) {
                        case 1:
                            
                            break;
                    
                        case 2:
                            
                            break;

                        case 3:
                            
                            break;

                        case 4:
                            
                            break;

                        case 5:
                            
                            break;

                        case 6:
                            
                            break;

                        case 7:
                            
                            break;

                        case 8:
                            
                            break;

                        case 9:
                            
                            break;

                        case 10:
                            
                            break;

                        case 11:
                        System.out.println("Regresando . . .\n");    
                            break;

                        default:
                        System.out.println("Menu Invalido!\n");
                        break;
                    }
                } while (opPerro !=11);
                    break;

                case 2:
                    break;

                case 3:
                System.out.println("Saliendo del programa . . .");
                    break;
            
                default:
                System.out.println("Menu inexistente");
                    break;
            }
        } while (op !=3);

        sc.close();

    }

    public static void menuPrincipal(){
        System.out.println("1) Perros");
        System.out.println("2) Gatos");
        System.out.println("3) Salir\n");
    }

    public static void menuPerro(){
        System.out.println("1) Agregar perro");
        System.out.println("2) Editar Perro");
        System.out.println("3) Eliminar Perro");
        System.out.println("4) Visualizar todos");
        System.out.println("5) Buscar por Id");
        System.out.println("6) Buscar por nombre");
        System.out.println("7) Buscar por dueño");
        System.out.println("8) Buscar por edad");
        System.out.println("9) Buscar por estado");
        System.out.println("10) Buscar por consulta");
        System.out.println("11) Volver al menu principal");
    }
}
