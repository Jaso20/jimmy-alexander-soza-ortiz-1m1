package Implements;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import FileConnection.RandomConnection;
import IDao.IDaoPerro;
import Pojo.Estado;
import Pojo.Perro;

public class PerroImplements implements IDaoPerro {
    private RandomConnection rcData;
    private RandomConnection rcHeader;
    private final String nameFile = "Perro.dat";
    private final String nameHeader= "HeaderPerro.dad";
    private final int SIZE = 137;
    private File filedata;
    private File fileheader;

    public PerroImplements() throws FileNotFoundException, IOException {
        init();
    }

    private void init() throws FileNotFoundException, IOException {
        filedata = new File(nameFile);
        fileheader = new File(nameHeader);
        rcData = new RandomConnection(filedata);
        rcHeader = new RandomConnection(fileheader);

        if (rcData.getConnection().length() == 0) {
            rcData.getConnection().seek(0);
        }

        if (rcHeader.getConnection().length() == 0) {
            rcData.getConnection().seek(0);
        }
    }

    @Override
    public void create(Perro t) throws FileNotFoundException, IOException {
        if (t == null){
            return ;
        }

        rcHeader.getConnection().seek(0);
        int n = rcHeader.getConnection().readInt();
        int k = rcHeader.getConnection().readInt();

        long pos = k * SIZE;

        rcData.getConnection().seek(pos);
        rcData.getConnection().writeInt(++k);

        rcData.getConnection().writeUTF(t.getNombre());
        rcData.getConnection().writeUTF(t.getDueño());
        rcData.getConnection().writeUTF(t.getColor());
        rcData.getConnection().writeDouble(t.getPeso());
        rcData.getConnection().writeInt(t.getEdad());
        rcData.getConnection().writeInt(t.getEstado().ordinal());
        rcData.getConnection().writeLong(t.getConsulta().toEpochDay());

        rcHeader.getConnection().seek(0);
        rcHeader.getConnection().writeInt(++n);
        rcHeader.getConnection().writeInt(k);

        long posHeader = 8 + (k - 1) * 4;

        rcHeader.getConnection().seek(posHeader);
        rcHeader.getConnection().writeInt(k);
        
        rcHeader.close();
        rcData.close();
        System.out.println("Perro registrado! \n");
    }

    @Override
    public void update(Perro t) {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete(Perro t) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Perro> findAll() throws FileNotFoundException, IOException {
        List<Perro> perros = new ArrayList<>();
        Perro p;

        rcHeader.getConnection().seek(0);
        int n = rcHeader.getConnection().readInt();

        for (int i=0 ; i < n ; i++){
            long posHeader = 8 + i * SIZE;

            rcHeader.getConnection().seek(posHeader);
            int key = rcHeader.getConnection.readInt();

            long posData = (key - 1) * SIZE;
            rcData.getConnection().seek(posData);
            
            p = new Perro();

            

        }


        return perros;
    }

    @Override
    public Perro findById(int id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Perro> findByName(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Perro> findByDueno(String dueno) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TreeSet<Perro> findByEdad(int edad) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Perro> findByEstado(Estado estatus) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Perro> consulta(LocalDate consulta) {
        // TODO Auto-generated method stub
        return null;
    }
}
