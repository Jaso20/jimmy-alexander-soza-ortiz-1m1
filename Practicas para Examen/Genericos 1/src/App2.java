public class App2 {
    public static void main(String[] args) {
        Integer[] enteros = {1,2,3,4,5};
        Double[] doubles = {1.1 , 2.2 , 3.3 , 4.4 , 5.5};
        Character[] chara = {'H','o','l','a'};

        printArray(enteros);
        printArray(doubles);
        printArray(chara);
    }

    public static <E> void printArray(E[] array){ //Metodo generico que recibe como elemento, un arreglo
        System.out.print("[");
        for (E e : array) {
            System.out.format("%5s",e);
        }
        System.out.println("]");
    }
}
