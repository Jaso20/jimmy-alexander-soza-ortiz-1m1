import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        List <Integer> enteros = new ArrayList<>();
        List <Double> doubles = new ArrayList<>();
        List <String> cadena = new ArrayList<>();

        Map<String,Integer> diccionario = new HashMap<>();

        Ciudadano[] p = {new Ciudadano("Jimmy", "Soza", 18, "2020-0889U") , new Ciudadano("Jeison", "Suarez", 20, "2020-1016U") , new Ciudadano("Jimmy", "Aburto", 20, "2018-1234U")};

        
        
        Scanner sc = new Scanner(System.in);

        listaE(enteros, sc); //Metodo para pedir valores enteros

        System.out.println("");

        listaD(doubles, sc); //Metodos para pedir valores dobles

        System.out.println("");

        listaS(cadena, sc); //Métodos para pedir cadenas 

        printArray(enteros);
        printArray(doubles);
        printArray(cadena);
        //printArray(p); Los genericos no pueden aceptar Objetos, a no ser que se hereden esos objetos

        System.out.println("");

        for (Ciudadano ciudadano : p) { //El diciconario guarda los nombres de los ciudadanos
            Integer valor = diccionario.get(ciudadano.getNombre());
            diccionario.put(ciudadano.nombre, key(valor));
        }

        System.out.println(diccionario.size() + " Ciudadanos con nombres distintos: ");
        System.out.println(diccionario);

    }

    public static Integer key(Integer valor) { // Metodo para que no haya "llaves" repetidas
        if (valor == null){
            valor = 1;
        }else{
            valor += valor;
        }
        return valor;
    }

    public static <E> void printArray(List<E> array){   //Arreglo generico que recibe como tipo, una colección
        System.out.print("[");
        for (E e : array) {
            System.out.format("%10s",e);
        }
        System.out.println("]");
    }

    public static void listaE(List<Integer> enteros, Scanner sc){
        for (int i=0 ; i < 2 ;i++){
            System.out.print("Ingrese un valor entero: ");
            enteros.add(sc.nextInt()); 
        } 
    }

    public static void listaD(List<Double> doubles, Scanner sc){
        for (int i=0 ; i < 2 ;i++){
            System.out.print("Ingrese valores decimales: ");
            doubles.add(sc.nextDouble()); 
        } 
    }

    public static void listaS(List<String> cadena, Scanner sc){
        sc.nextLine();
        for (int i=0 ; i < 2 ;i++){
            System.out.print("Ingrese cadenas: ");
            cadena.add(sc.nextLine()); 
        } 
    }

    

    
}
