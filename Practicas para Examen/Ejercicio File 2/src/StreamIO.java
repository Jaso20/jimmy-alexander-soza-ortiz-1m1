import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class StreamIO {
    public static final String System = null;
	private FileInputStream fis;
    private FileOutputStream fos;
    private File file;

    public void StreamIO(File file) throws FileNotFoundException {
        this.file = file;
    }

    public void StreamIO(String filename) throws FileNotFoundException {
        this.file = new File(filename);
    }

    public void write(String text) throws IOException{
        fos = new FileOutputStream(file,true);
        fos.write(text.getBytes());
    }

    public String read() throws IOException{
        fis = new FileInputStream(file);
        int n;
        String text ="";

        while ((n =fis.read()) != -1){
            text += (char) n;
        }

        return text;
    }

    public void close() throws IOException{
        if (fis != null){
            fis.close();
        }

        if (fos != null){
            fos.close();
        }
    }

    public StreamIO() {
    }

}
