import java.util.Scanner;

/*La facultad de Ciencias y Sistemas necesita llevar el control de sus Activos Fijos 
(Código, nombre, descripción, cantidad, tipo de activo fijo (clasificación), valor y  fecha de compra)
y la depreciación de los mismos
los activos fijos están clasificados en: Vehículos, equipos de computo, mobiliario y edificio,
cada uno de los cuales se deprecian en 5, 2, 3 y 20 años respectivamente. 
La aplicación que usted va a desarrollar deberá tener el siguiente menú de opciones:

1. Agregar nuevo activo fijo

2. Visualizar todos los activos fijos

3. Calcular depreciación de todos los activos fijos.

4. Salir

Para el caso del método a utilizar para calcular la depreciación, la aplicación deberá preguntar al usuario que tipo de método podrá utilizar entre Linea Recta y Sistema de Dígito de los años incremental.

Restricciones:

No puede usar la clase ArrayList o alguna clase de tipo Colección.
Debe implementar arreglos dinámicos.
Deben usar Programación Orientada a Objetos.
Sólo revisaré programas que ejecuten y resuelvan el problema, no valoraré parte de código.
El programa deberá imprimir el activo fijo y su depreciación por año y al final deberá totalizar la depreciación de todos los activos por año*/

public class App {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        Services afServices = new Services();
        int op = 0;

        do {
            System.out.println("\t--- MENU ---\n");
            menu();
            op = sc.nextInt();
            switch (op) {
                case 1:
                    afServices.add();
                    break;
            
                case 2:
                afServices.findAll();
                    break;

                case 3:
                afServices.depreciate();
                    break;

                case 4:
                System.out.println("Cerrando programa...");
                sc.close();
                break;

                default:
                System.out.println("Menu Inexistente!");
            }
            
        } while (op!=4);
    }

    public static void menu(){
        System.out.println("1) Agregar nuevo activo fijo");
        System.out.println("2) Visualizar todos los activos fijos");
        System.out.println("3) Calcular depreciacion de todos los activos fijos");
        System.out.println("4) Salir\n");
    }
}
