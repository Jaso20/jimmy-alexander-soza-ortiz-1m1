import java.util.Scanner;

public class Services implements Idao {
    Scanner sc = new Scanner(System.in);
    Model arrayD = new Model();
    ActivoFijo af;
    ActivoFijo[] lista = null;

    @Override
    public void add() {
        int codigo;
        String nombre;
        String descripcion;
        int cantidad;
        TipoActivoFijo clasificacion = null;
        double valor;
        String fechaCompra;
        double depreciacion = 0;


        System.out.print("Ingrese el codigo:\t");
        codigo = sc.nextInt();

        sc.nextLine();

        System.out.print("Ingrese el nombre:\t");
        nombre = sc.nextLine();

        System.out.print("Ingrese la descripcion:\t");
        descripcion = sc.nextLine();

        System.out.print("Ingrese la cantidad:\t");
        cantidad = sc.nextInt();

        System.out.print("Ingrese el valor del activo fijo:\t");
        valor = sc.nextDouble();

        int clasi;
        do{
            System.out.println("\nIngrese el tipo de Activo Fijo");
            menuTipoActivoFijo();
            clasi = sc.nextInt();

            switch (clasi) {
                case 1:
                clasificacion = TipoActivoFijo.Vehiculo;
                depreciacion = valor / 5;
                break;
        
                case 2:
                clasificacion = TipoActivoFijo.Equipo_de_computo;
                depreciacion = valor / 2;

                break;

                case 3:
                clasificacion = TipoActivoFijo.Mobiliario;
                depreciacion = valor / 3;

                break;

                case 4:
                clasificacion = TipoActivoFijo.Edificio;
                depreciacion = valor / 20;

                break;

                default:
                System.out.println("Clasificacion invalida!\n");
            }

        }while ((clasi < 1) || (clasi > 4));

        sc.nextLine();
        System.out.print("Ingrese la fecha de compra:\t");
        fechaCompra = sc.nextLine();

        af = new ActivoFijo(codigo, nombre, descripcion, cantidad, clasificacion, valor, fechaCompra,depreciacion);
        lista = arrayD.dinamic(lista, af);
       //lista[lista.length] = af;

        System.out.println("Activo Fijo registrado exitosamente!!!");
    }

    @Override
    public void findAll() {
        header();
        for (ActivoFijo af : lista) {
            print(af);
        }
    }

    @Override
    public void depreciate() {
        int op = 0;
        menuDepre();
        op = sc.nextInt();

        switch (op){
            case 1:
            menu1();
            break;

            case 2:
            menu2();
            break;
        }

    }

    public void menuDepre(){
        System.out.println("Por cual metodo desea calcula la depreciacion?");
        System.out.println("1) Linea Recta");
        System.out.println("2) Sistema de Digitos de los años incremental\n");
    }

    public void menu1(){
        String[] nombre = new String[lista.length];
        int m = 0;

        for (ActivoFijo name : lista) {
            nombre[m] = lista[m].getNombre();
            m++;
        }

        System.out.format("%15s","Activo Fijo");
        for (int i = 0; i <=20 ; i++) {
            System.out.format("%2s\t",i);
        }

        System.out.println("");

        for (int i = 0; i < lista.length; i++) {
            double valor2 = lista[i].getDepreciacion();

            do {
                System.out.format("%15s %2s\n",nombre[i],lista[i].getDepreciacion());
                valor2 += valor2;
            } while (lista[i].getValor() == valor2);
        }
    }

    public void menu2(){

    }

    public static void menuTipoActivoFijo(){
        System.out.println("1) Vehiculo");
        System.out.println("2) Equipo de Computo");
        System.out.println("3) Mobiliario");
        System.out.println("4) Edificio\n");
    }

    public static void header(){
        System.out.format("%5s %20s %40s %20s %20s %20s %20s\n %1s\n","Codigo","Nombre","Descripcion","Cantidad","Tipo","Valor","Fecha de Compra","------------------------------------------------------------------------------------------------------------------------------------------------------------");
    }

    public static void print(ActivoFijo af){
        System.out.format("%5s %20s %40s %20s %20s %20s %20s\n",af.getCodigo(),af.getNombre(),af.getDescripcion(),af.getCantidad(),af.getClasificacion(),af.getValor(),af.getFechaCompra());
    }
    
}
