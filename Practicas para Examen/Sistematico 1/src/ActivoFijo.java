public class ActivoFijo {
    private int codigo;
    private String nombre;
    private String descripcion;
    private int cantidad;
    private TipoActivoFijo clasificacion;
    private double valor;
    private String fechaCompra;
    private double depreciacion;

    public ActivoFijo(int codigo, String nombre, String descripcion, int cantidad, TipoActivoFijo clasificacion,
            double valor, String fechaCompra,double depreciacion) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.clasificacion = clasificacion;
        this.valor = valor;
        this.fechaCompra = fechaCompra;
        this.depreciacion = depreciacion;
    }


    public ActivoFijo() {
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public TipoActivoFijo getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(TipoActivoFijo clasificacion) {
        this.clasificacion = clasificacion;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(String fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public void setDepreciacion(double depreciacion) {
        this.depreciacion = depreciacion;
    }

    public double getDepreciacion() {
        return depreciacion;
    }
    
    
}
