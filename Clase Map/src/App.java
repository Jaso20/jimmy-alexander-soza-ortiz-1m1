import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class App {
    public static void main(String[] args) throws Exception {
        Estudiante e1 = new Estudiante(1, "Jimmy" , "Soza", 18, new Date());
        Estudiante e2 = new Estudiante(2, "Jeison" , "Suarez", 20, new Date());
        Estudiante e3 = new Estudiante(3, "Jairo" , "Aburto", 20, new Date());

        Map<Integer, Estudiante>  lista = new HashMap<Integer,Estudiante>();

        lista.put(1, e1);
        lista.put(2, e2);
        lista.put(3, e3);

        for (Entry<Integer,Estudiante> e : lista.entrySet()) { //El metodo entry, es el tipo de elemento que recibira y el metodo lista,entrySet()
            System.out.print("Codigo: " + e.getKey() + "\t");  // devuele la Key junto al objeto "Estudiante"
            System.out.println(e.getValue().toString());
        }
        //Se sobreescribe el metodo ToString, para que no te devuelva el Hash
    }
}
