import java.util.Date;

public class Estudiante {
    private int id;
    private String nombre;
    private String apellido;
    private int edad;
    private Date fecha_ingreso;

    public Estudiante() {
    }

    public Estudiante(int id, String nombre, String apellido, int edad, Date fecha_ingreso) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.fecha_ingreso = fecha_ingreso;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Date getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(Date fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    @Override
    public String toString() {
        return "Id: " +id + " Nombre: " + nombre + " Apellido: " + apellido + " Edad: " + edad + " fecha de ingreso: " + fecha_ingreso;
    }
}
