import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class App {
    public static void main(String[] args) throws Exception {
        Integer[] numeros = {2 , 34 , 5 , 100 , 64};
        String[] cadena = {"Hola","buenas","tardes","nos vemos","luego"};
        Character[] chara = {'H','o','l','a'};
        Double[] decimal = {0.5 , 0.8 , 0.2 , 120.5 , 10.6 , 25.6};

        System.out.println("\n");

        TreeSet<Integer> ordenarInteger =  new TreeSet<>(); //El TreeSet, se encarga de ordenar los elementos que van ingresando al arreglo

        ordenarInteger = addTreeset(numeros, ordenarInteger);
        
        System.out.println("Arreglo sin ordenar: \n " + imprimirArray(numeros) );
        System.out.println("Arreglo ordenado: \n " + ordenarInteger);

        System.out.println("");

        TreeSet<String> ordenarString = new TreeSet<>();
        ordenarString = addTreeset(cadena, ordenarString);

        System.out.println("Arreglo cadenas sin ordenar \n" + imprimirArray(cadena));
        System.out.println("Arreglo de cadenas ordenado\n" + ordenarString);

        System.out.println("");

        TreeSet<Character> ordenarChara= new TreeSet<>();
        ordenarChara = addTreeset(chara, ordenarChara);

        System.out.println("Arreglo caracter sin ordenar \n" +imprimirArray(chara));
        System.out.println("Arreglo caracter ordenado\n" + ordenarChara);

        System.out.println("");

        TreeSet<Double> ordenarDouble= new TreeSet<>();
        ordenarDouble = addTreeset(decimal, ordenarDouble);

        System.out.println("Arreglo caracter sin ordenar \n" +imprimirArray(decimal));
        System.out.println("Arreglo caracter ordenado\n" + ordenarDouble);
    }

    /*private static List<Integer> getArray(Integer[] array) {
        List<Integer> lista = new ArrayList<Integer>();
        for (Integer num : array) {
            lista.add(num);
        }
        return lista;
    }*/

    private static <T> List<T> imprimirArray(T[] array) {
        List<T> lista = new ArrayList<>();
        for (T t : array) {
            lista.add(t);
        }
        return lista;
    }

    private static <T> TreeSet<T> addTreeset(T[] array , TreeSet<T> sort){
        for (T t : array) {
            sort.add(t);
        }
        return sort;
    }
}
