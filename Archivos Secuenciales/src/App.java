import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        final String NAMEFILE = "Hola.txt";
        File file = new File(NAMEFILE);;
        FileInputStream fis = null;
        FileOutputStream fos = null;
        Scanner entrada = new Scanner(System.in);
        String texto;

        entrada.nextLine();
        System.out.println("Ingrese una linea de texto");
        texto = entrada.nextLine();

        try {
            fos = new FileOutputStream(file,false);
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            fos.write(texto.getBytes());   // Manda a guardar la linea de texto en el txt
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\nEn el texto se encuentra . . .");

        int n;
        String text = "";
        try {
            while ((n = fis.read()) != -1) { // Devolver lo que se encuentra en el txt
                text += (char) n;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(text);

    }
}
