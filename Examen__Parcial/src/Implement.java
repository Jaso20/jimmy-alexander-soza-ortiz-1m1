import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Implement {
    FileConnection fc;

    public Implement() throws FileNotFoundException {
        fc = new FileConnection();
    }

    public Map<String, Integer> ver(String text) throws IOException {
        HashMap<String,Integer> dic = new HashMap<String,Integer>();
        String[] palabras;
        List<String> lista = new ArrayList<>();
    
        text = text.replaceAll("[\\.\\,\\(\\)]", "");
        palabras = text.split(" ");
        
        for (String string : palabras) {
            lista.add(string);
        }

        for (String palabra : palabras){
            if (dic.containsKey(palabra)) {
                dic.put(palabra, dic.get(palabra) + 1);
            } else {
                dic.put(palabra, 1);
            }
        }

        return dic;
    }

    public String find() throws IOException {
        String texto;
        String text = "";
        texto = printText(text);
        return texto;
    }

    public  String printText(String text) throws IOException {
        int n;
        while((n = fc.fis.read()) != -1){
            text += (char) n;
        }
        return text;
    }

    
}
