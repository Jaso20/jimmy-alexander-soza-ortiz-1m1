import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class FileConnection {
    FileInputStream fis;
    File file;
    private static final String FILENAME = "examen1.txt";
    static Scanner sc = new Scanner(System.in);

    public FileConnection() throws FileNotFoundException {
        load();
    }

    private void load() throws FileNotFoundException {
        file = new File(FILENAME);
        fis = new FileInputStream(file);
    }

    public void close() throws IOException {
        if (fis != null){
            fis.close();
        }
    }

}
